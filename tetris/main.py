#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 12 11:57:50 2021

@author: varanets
Версия с ипользованием базы данных PostgreSQL
"""
from string import ascii_letters  # используются для генерации токенов
from random import choice, randint  # используются для генерации токенов
from datetime import datetime  # используется для установки времени сессии
# from sys import argv
from socket import gethostname
import psycopg2
import psycopg2.extras
from flask import Flask
from flask import render_template, request, redirect, url_for, session, flash
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
app.secret_key = "I5WW7tbX6JBniDvuFicKmVVFAdZrMcd9M"
db = {"host": "172.36.0.2", "user": "python", "password": "passwd",
      "dbname": "game"}
server = gethostname()
# if len(argv) > 1:
#    server = argv[1]
# else:
#    server = ""


def dbquery(query: str, answer=0):
    """
    Функция для соединения с БД. В качестве аргумента принимает строку запроса
    и целое число, характеризующее запрос:
    0 - запись в БД (возвращает None)
    1 - ожидается единичная запись из БД (возвращает кортеж полей)
    2 и более - ожидается множество записей из БД (возвращает список кортежей)
    """
    with psycopg2.connect(**db) as sqlconn:
        sqlcur = sqlconn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        sqlcur.execute(query)
        if answer:
            if answer == 1:
                row = sqlcur.fetchone()
            else:
                row = sqlcur.fetchall()
            return row
        else:
            sqlconn.commit()
            return None


def create_session(user):
    '''
    Функция генерирует токен случайной длины для идентификации сессии
    пользователя (в частности, с помощью неё сгенерирован secret_key
    этого приложения). После чего записывает токен в cookie клиента и
    сохраняет в качестве ключа словаря на сервере (в качестве значения
    передаём срок окончания сессии, имя пользователя и стартовый рекорд сессии)
    '''
    seq = ascii_letters + '0123456789'
    token = ''
    for _ in range(randint(30, 50)):
        token += choice(seq)
    # Ограничиваем время бездействия в течение сессии одними сутками
    time = int(datetime.now().timestamp()) + 86400
    dbquery("INSERT INTO tokens (token, username, valid_until) VALUES \
            ('%s', '%s', %d)" % (token, user, time))
    session['token'] = token


def verify_session():
    '''
    Функция проверяет авторизацию пользователя. Если у пользователя открыта
    сессия с существующим токеном и время бездействия пользователя составляет
    не больше суток, то обновляем время и в качестве ответа возвращает имя
    пользователя.
    Если время бездействия больше суток, удаляем сессию и возвращаем None
    Если же сессии нет или она не зарегистрирована (например, из-за перезапуска
    сервера), то функция возвращает None
    '''
    current_user = None
    if 'token' in session:
        token = session.get('token')
        row = dbquery("SELECT * FROM tokens WHERE token = '%s'" % token, 1)
        if row:
            time = int(datetime.now().timestamp())
            if row['valid_until'] > time:  # если сессия ещё не "протухла"
                time += 86400
                # обновляем время сессии
                dbquery("UPDATE tokens SET valid_until = %d \
                        WHERE token = '%s'" % (time, token))
                current_user = dict(row)
                # print(current_user)
            else:  # иначе удаляем сессию
                session.pop('token', None)
                dbquery("DELETE FROM tokens WHERE token = '%s'" % token)
    return current_user


MES1 = "Поздравляем! Вы установили новый рекорд в текущей сессии: %d очков."
MES2 = "ПОЗДРАВЛЯЕМ! Вы побили свой собственный рекорд! Теперь он равен \
   %d очков. Предыдущий рекорд был равен %d очков."


@app.route('/', methods=['GET', 'POST'])
def index():
    '''
    Функция проверяет наличие текущей сессии в списке открытых. Если сессия
    существует, то отображает игру. После каждой игры получает результат и
    заносит его в список. Затем сверяет текущие результаты с рекордом сессии
    и рекордом пользователя, и если рекорд побит, то выводит соответствующее
    уведомление.
    '''
    result = verify_session()
    if result:
        if request.method == 'POST':
            time = int(datetime.now().timestamp())
            score = int(request.form.get("score"))
            dbquery("INSERT INTO records (name, score, ts) VALUES \
                    ('%s', '%s', %d)" % (result['username'], score, time))
            if score > result['record']:
                dbquery("UPDATE tokens SET record = %d \
                        WHERE token = '%s'" % (score, result['token']))
                flash(MES1 % score)
                row = dbquery("SELECT * FROM users WHERE name = '%s'" %
                              result['username'], 1)
                if score > row['record_score']:
                    flash(MES2 % (score, row['record_score']))
                    dbquery("UPDATE users SET record_score = %d, record_ts =%d\
                            WHERE name = '%s'" % (score, time,
                            result['username']))
            return render_template('index.html', user=result['username'],
                                   server=server)
        else:
            return render_template('index.html', user=result['username'],
                                   server=server)
    else:
        return redirect(url_for('login'))


@app.route('/table')
def table():
    '''
    Если пользователь зарегистрирован, то отображаем ему:
    - лучший результат в текущей сессии
    - список пользователей с их максимальным рекордом
    - Top-10 лучших результатов
    '''
    result = verify_session()
    if result:
        userscore = []
        rows = dbquery("SELECT * FROM users", 2)
        for i in rows:
            time = datetime.fromtimestamp(i['record_ts'])
            time = time.strftime("%d/%m/%Y %X")
            userscore.append((i['name'], i['record_score'], time))
        top10 = []
        rows = dbquery("SELECT * FROM records ORDER BY score DESC LIMIT 10", 2)
        for i in rows:
            time = datetime.fromtimestamp(i['ts'])
            time = time.strftime("%d/%m/%Y %X")
            top10.append((i['name'], i['score'], time))
        return render_template('table.html', user=result['username'],
                               session=result['record'], userscore=userscore,
                               top10=top10, name='Рекорды', server=server)
    else:
        return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    '''
    Функция осуществляет авторизацию пользователя. Если нам присланны верные
    данные (которые совпадают с данными из словаря пользователей), то вызываем
    функцию создания сессии (генерирует случайный токен; одна копия сохраняется
    в словарь сессий вместе с именем пользователя, другая записывается в cookie
    на стороне пользователя). После авторизации пользователь перенаправляется
    на главную страницу. Если введены неверные данные, то отображается та же
    самая страница с сообщением ошибке.
    '''
    err = 'Неправильный логин или пароль'
    if request.method == 'POST':
        user = request.form.get("login")
        password = request.form.get("password")
        row = dbquery("SELECT * FROM users WHERE name = '%s'" % user, 1)
        if row:
            if check_password_hash(row['password'], password):
                create_session(user)
                return redirect(url_for('index'))
            else:
                flash(err)
                return render_template('login.html', name='Вход',
                                       server=server)
        else:
            flash(err)
            return render_template('login.html', name='Вход', server=server)
    else:
        return render_template('login.html', name='Вход', server=server)


@app.route('/registry', methods=['GET', 'POST'])
def registry():
    '''
    Функция создаёт нового пользователя. Проверяется уникальность и длина имени
    и совпадение и длина паролей. Если введены верные данные, то пользователь
    добавляется и для него автоматически создаётся сессия; происходит
    перенаправление на главную страницу. Если нет, то отображается текущая
    страница с подстановкой введённых пользователем данных.
    '''
    if request.method == 'POST':
        credent = [request.form.get("login"), request.form.get("password1"),
                   request.form.get("password2")]
        row = dbquery("SELECT * FROM users WHERE name = '%s'" % credent[0], 1)
        if not row and len(credent[0]) > 3:
            if credent[1] == credent[2] and len(credent[1]) > 5:
                password = generate_password_hash(credent[1])
                dbquery("INSERT INTO users (name, password) VALUES \
                        ('%s', '%s')" % (credent[0], password))
                create_session(credent[0])
                return redirect(url_for('index'))
            else:
                flash("Пароли не совпадают или слишком короткий пароль")
                return render_template('registry.html', name='Регистрация',
                                       credent=credent, server=server)
        else:
            flash("Слишком короткое имя или такой пользователь уже существует")
            return render_template('registry.html', name='Регистрация',
                                   credent=credent, server=server)
    else:
        return render_template('registry.html', name='Регистрация',
                               server=server)


@app.route('/chpasswd', methods=['GET', 'POST'])
def chpasswd():
    '''
    При наличии действующей сессии мы можем поменять пароль текущему
    пользователю. Сначала мы проверяем правильность введённого старого пароля,
    затем проверяем соответствие новых (что они совпадают и соответствуют по
    длине). В случае правильности введённых данных, обновляем пароль и
    переводим на главную страницу в рамках текущей сессии. В случае ошибок,
    отображается текущая страница с подстановкой введённых данных.
    '''
    result = verify_session()
    if result:
        if request.method == 'POST':
            credent = [request.form.get("password0"),
                       request.form.get("password1"),
                       request.form.get("password2")]
            row = dbquery("SELECT * FROM users WHERE name = '%s'" %
                          result['username'], 1)
            if check_password_hash(row['password'], credent[0]):
                if credent[1] == credent[2] and len(credent[1]) > 5:
                    password = generate_password_hash(credent[1])
                    dbquery("UPDATE users SET password = '%s' WHERE \
                            name = '%s'" % (password, result['username']))
                    return redirect(url_for('index'))
                else:
                    flash("Пароли не совпадают или слишком короткий пароль")
                    return render_template('chpasswd.html', credent=credent,
                                           name='Смена пароля', server=server,
                                           user=result['username'])
            else:
                flash("Ошибка при проверке пароля")
                return render_template('chpasswd.html', name='Смена пароля',
                                       user=result['username'], server=server,
                                       credent=credent)
        else:
            return render_template('chpasswd.html', user=result['username'],
                                   name='Смена пароля', server=server)
    else:
        return redirect(url_for('login'))


@app.route('/logout')
def logout():
    '''
    Функция осуществляет закрытие сессии. Если существует действующая сессия,
    то удаляется токен на стороне пользователя, по нему находится сессия,
    расположенная в словаре на стороне сервера и также удаляется. Пользователь
    переводится на страницу авторизации.
    '''
    if verify_session():
        token = session.pop('token', None)
        dbquery("DELETE FROM tokens WHERE token = '%s'" % token)
    return redirect(url_for('login'))


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
