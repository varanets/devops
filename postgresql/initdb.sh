#! /bin/bash
set -e
echo "host game python 172.36.0.0/24 md5" >> /var/lib/postgresql/data/pg_hba.conf
psql -c "CREATE USER python PASSWORD 'passwd';"
psql -c "CREATE DATABASE game OWNER python;"
psql -d game < /srv/dump.sql 



