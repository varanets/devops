--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: records; Type: TABLE; Schema: public; Owner: python
--

CREATE TABLE public.records (
    name character varying(20) NOT NULL,
    score integer NOT NULL,
    ts integer
);


ALTER TABLE public.records OWNER TO python;

--
-- Name: tokens; Type: TABLE; Schema: public; Owner: python
--

CREATE TABLE public.tokens (
    token character varying(50) NOT NULL,
    username character varying(20) NOT NULL,
    valid_until integer NOT NULL,
    record integer DEFAULT 0
);


ALTER TABLE public.tokens OWNER TO python;

--
-- Name: users; Type: TABLE; Schema: public; Owner: python
--

CREATE TABLE public.users (
    name character varying(20) NOT NULL,
    password character varying(150) NOT NULL,
    record_score integer DEFAULT 0 NOT NULL,
    record_ts integer DEFAULT 0
);


ALTER TABLE public.users OWNER TO python;

--
-- Data for Name: records; Type: TABLE DATA; Schema: public; Owner: python
--

INSERT INTO public.records VALUES ('guest', 48, 1638643919);
INSERT INTO public.records VALUES ('guest', 48, 1638644014);
INSERT INTO public.records VALUES ('guest', 48, 1638644067);
INSERT INTO public.records VALUES ('varanets', 85, 1638644339);
INSERT INTO public.records VALUES ('enotochka', 26748, 1638645260);


--
-- Data for Name: tokens; Type: TABLE DATA; Schema: public; Owner: python
--



--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: python
--

INSERT INTO public.users VALUES ('guest', 'pbkdf2:sha256:260000$VBVm1ZOWbrAxabRA$364a584d97ed6cdb0eceac0dc7d185f1ac7eeb38ab8345dd69cd32a02f3a2274', 48, 1638644067);
INSERT INTO public.users VALUES ('varanets', 'pbkdf2:sha256:260000$R0O2UboRGuujuSA7$4e204537ccb9e36c8e3d86b4c58ec616f300b48044fb7bfa71133fd3ac8a6436', 85, 1638644339);
INSERT INTO public.users VALUES ('enotochka', 'pbkdf2:sha256:260000$69qKVpwPIthflF6l$1e4af24caa1cd1158696e697577fe2e5ee2aa8ff014ef3d0b40264624177cb0b', 26748, 1638645260);


--
-- Name: tokens tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: python
--

ALTER TABLE ONLY public.tokens
    ADD CONSTRAINT tokens_pkey PRIMARY KEY (token);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: python
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (name);


--
-- Name: records records_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: python
--

ALTER TABLE ONLY public.records
    ADD CONSTRAINT records_name_fkey FOREIGN KEY (name) REFERENCES public.users(name);


--
-- Name: tokens tokens_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: python
--

ALTER TABLE ONLY public.tokens
    ADD CONSTRAINT tokens_user_fkey FOREIGN KEY (username) REFERENCES public.users(name);


--
-- Name: TABLE records; Type: ACL; Schema: public; Owner: python
--

GRANT ALL ON TABLE public.records TO python;


--
-- Name: TABLE tokens; Type: ACL; Schema: public; Owner: python
--

GRANT ALL ON TABLE public.tokens TO python;


--
-- Name: TABLE users; Type: ACL; Schema: public; Owner: python
--

GRANT ALL ON TABLE public.users TO python;


--
-- PostgreSQL database dump complete
--

